#ifndef INCLUDE_TIM
#define INCLUDE_TIM

void TIMsetup();
void Set_stepCycle(int* SC);

#endif

/*
#define TIM0_Cycle 200000　割り込み周期をusで設定

「IRAM_ATTR tim」関数内に処理を記入

 timerAlarmEnable(timer0); タイマ0有効化
 timerAlarmDisable(timer0);タイマ0無効化

*/