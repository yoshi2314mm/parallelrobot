
#include <Arduino.h>
#include "blynkBLE.h"

#include "tim.h"
#include "lifter.h"

//blynk
#define BLYNK_PRINT Serial
#define BLYNK_USE_DIRECT_CONNECT
#include "BlynkSimpleEsp32_BLE.h"
#include <BLEDevice.h>
#include <BLEServer.h>
char auth[] = "nN9Z0LtcRop8-RKGOUxUnU9iF5LW-Ytc";


//EndEffecter parameter
int Xew,Yew,Zew=0;
int Sx,Sy;

int reset=0;

void blynkSetup(){
  Serial.println("Waiting for connections...");
  Blynk.setDeviceName("Blynk");
  Blynk.begin(auth);
}

void blynkRead(){
  Blynk.run();
}

BLYNK_WRITE(V0) { 
  Serial.print("Sx=");
  Serial.print(param.asInt());
  Sx=param.asInt();
}

BLYNK_WRITE(V1) { 
  Serial.print("Sy=");
  Serial.println(param.asInt());
  Sy=param.asInt();
}
BLYNK_WRITE(V2) { 
  Serial.print("Xew=");
  Serial.print(param.asInt());
  Xew=param.asInt();
}

BLYNK_WRITE(V3) { 
  Serial.print("Yew=");
  Serial.println(param.asInt());
  Yew=param.asInt();
}
BLYNK_WRITE(V4) { 
  Serial.print("Zew=");
  Serial.println(param.asInt());
  Zew=param.asInt();
}
BLYNK_WRITE(V5) { 
    if(reset==0){
  Serial.print("reset");
  TIMsetup();
  reset=1;
    }
}

int* BlynkState(void) {
    int* ip = NULL;
    ip = (int*)malloc(sizeof(int) * 5);

    ip[0] = Zew;
    ip[1] = Zew;
    ip[2] = Zew;
    ip[3] = Zew;
    ip[4] = Zew;
    
  
    return ip;
}