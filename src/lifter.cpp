#include <Arduino.h>
#include "lifter.h"
#include "blynkBLE.h"






/*
定数
可動範囲
最大速度
最小速度
加速度

変数
現在位置
現在速度
目標位置

判別
速度方向
加速方向
回転有効
*/
#define NoM 2

#define MovableRange 200*133 //
#define Acce 2*133
#define Vmax 10*133
#define Vmin 0.1*133
#define InitialSpeed 10*133

#define PermissibleRange 2000 //目標範囲　（目標にある程度近づいたら停止
#define RestartRange 3000　//追従再開距離　（目標から離れると追従を再開
char arrival_flag[NoM]; //目標範囲内なら0


int InitialHeight=5;


int32_t Pcur[NoM]={10000,10000};//現在位置
uint32_t Ptar[NoM];
int32_t Pdif[NoM];
int8_t dir[NoM]={1,-1};

int16_t Vcur[NoM];
int16_t Vtar[NoM];
uint16_t pulse_fre[NoM];


char LifterInitialization;
char sw[NoM];
uint8_t sw_PIN[]={35,34};

uint8_t step_PIN[]={26,25};
uint8_t dir_PIN[]={32,33};



const float mmP=(200*1/3*2); //mm->pulse    pulsu/cycle * microstep * leadscrew * toggle

void Pcalc(uint32_t pulse_CNT[]){

//timからパルス生成回数を取得して現在位置を更新
 Serial.print("Pcur:"); 
  for (uint8_t i = 0; i < NoM; i++){
    Pcur[i]+=dir[i]*pulse_CNT[i];
    Serial.print(Pcur[i]);
     Serial.print(",");
  }
  Serial.println();


//blynkから目標位置を取得
int* arr=NULL;
  arr=BlynkState();
   //Serial.print("Ptar:");
   
    for (uint8_t i = 0; i < NoM; i++){
      Ptar[i]=arr[i]*133; //mm->pulse
       //Serial.print(Ptar[i]);
        //Serial.print(",");
    }
      //Serial.println();
      
  free(arr);


 for (uint8_t i = 0; i < NoM; i++){
  Pdif[i]=Ptar[i]-Pcur[i];  //目標までの距離

 int8_t TargetDirection;
 if((Pdif[i])>0){

  TargetDirection=1;
  }
  else{

   TargetDirection=-1;
  }


/*
if(arrival_flag[i]==1 && abs(Pdif[i])<500){
  arrival_flag[i]=0;
}
if(arrival_flag[i]==0 && abs(Pdif[i]) >1000){
  arrival_flag[i]=1;
}
*/
if(abs(Vcur[i])<5) arrival_flag[i]=0;
else arrival_flag[i]=1;


 
   
  int32_t deltaVmax=TargetDirection*sqrt((2*Acce*abs(Pdif[i])+Vcur[i]*Vcur[i]+Vtar[i]*Vtar[i])/2);//三角速度波形時の最大速度
  /*
        Serial.print("calc");
        Serial.print(",");
        Serial.print(TargetDirection);
        Serial.print(",");
        Serial.println(deltaVmax);
*/




//次速度
  if(deltaVmax>Vcur[i])Vcur[i]+=0.01*Acce; 
  if(deltaVmax<Vcur[i])Vcur[i]-=0.01*Acce;

 if(abs(Vcur[i])>Vmax)Vcur[i]=dir[i]*Vmax; //速度上限設定
 /*
 Serial.print("Vcur");
        Serial.print(",");
         Serial.print(Vcur[i]);
         */

 

        if(abs(Vcur[i])>10)  pulse_fre[i]=106700/abs(Vcur[i]);
        /*
         Serial.print("pulse F");
        Serial.print(",");
        Serial.println(pulse_fre[i]);
        */
       

  if(Vcur[i]>0){
    dir[i]=1;  //現在位置加算方向指定
    digitalWrite(dir_PIN[i],0); //速度方向指定ピン設定　
  }
  else{
    dir[i]=-1;
    digitalWrite(dir_PIN[i],1);
  } 



 }
}


void pulse(uint8_t i){
  if(arrival_flag[i]==1){
    digitalWrite(step_PIN[i], !digitalRead(step_PIN[i]));
   
  }
}



void lifter_setup(){
  for (uint8_t i = 0; i < NoM; i++){
pinMode(step_PIN[i], OUTPUT);
pinMode(dir_PIN[i], OUTPUT);
Pcur[i]=0;
Ptar[i]=0;
Vcur[i]=0;
Vtar[i]=0;
pulse_fre[i]=35;
  }
}

int* MovementPermit(void) {
   
    int* ip = NULL;
    ip = (int*)malloc(sizeof(int) * 4);

    for (uint8_t i = 0; i < NoM; i++) ip[i]=arrival_flag[i];


    for (uint8_t i = 0; i < NoM; i++) ip[NoM+i]=(int)pulse_fre[i];
      
    return ip;
}


