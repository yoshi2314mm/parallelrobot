#include <Arduino.h>
#include "tim.h"
#include "lifter.h"

#define NoM 2
uint32_t step_CNT[NoM];
uint32_t pulse_CNT[NoM];
volatile uint16_t reset_Cycle[NoM];
uint8_t step_EN[NoM];




uint32_t cnt;


//interrupt cycle (us)
#define TIM0_Cycle 397
#define TIM1_Cycle 10000
#define TIM2_Cycle 1000000
#define TIM3_Cycle 1000000


hw_timer_t * timer0 = NULL;
hw_timer_t * timer1 = NULL;
hw_timer_t * timer2 = NULL;
hw_timer_t * timer3 = NULL;



void IRAM_ATTR tim0() { //MD用パルス生成

 for (uint8_t i = 0; i < NoM; i++){
 step_CNT[i]+=step_EN[i];
 if(step_CNT[i]>reset_Cycle[i]){
   step_CNT[i]=0;
   pulse_CNT[i]++;
 pulse(i);
 }

}
 
 cnt++;
}


void IRAM_ATTR tim1() {//速度計算

//printf("B %d,%d\r\n",pulse_CNT[0],pulse_CNT[1]);
Pcalc(pulse_CNT);

  int* array=NULL;
  array= MovementPermit();

for (uint8_t i = 0; i < NoM; i++){
  pulse_CNT[i]=0;
  step_EN[i]=array[i];
}

for (uint8_t i = 0; i < NoM; i++)reset_Cycle[i]=array[NoM+i];

  free(array);

}

void IRAM_ATTR tim2() {
    Serial.print("tim0 fre=");
  Serial.println(cnt);
  cnt=0;
}
void IRAM_ATTR tim3() {
}

void TIMsetup() {
  //
lifter_setup();

for (uint8_t i = 0; i < NoM; i++){
reset_Cycle[i]=50;
 step_CNT[i]=0;
step_EN[i]=0;
  }


  timer0 = timerBegin(0, 1, true);
  timerAttachInterrupt(timer0, &tim0, true);
  timerAlarmWrite(timer0, TIM0_Cycle, true);
  timerAlarmEnable(timer0);

  timer1 = timerBegin(1, 80, true);
  timerAttachInterrupt(timer1, &tim1, true);
  timerAlarmWrite(timer1, TIM1_Cycle, true);
  timerAlarmEnable(timer1);


  timer2 = timerBegin(2, 80, true);
  timerAttachInterrupt(timer2, &tim2, true);
  timerAlarmWrite(timer2, TIM2_Cycle, true);
  //timerAlarmEnable(timer2);

  timer3 = timerBegin(3, 89, true);
  timerAttachInterrupt(timer3, &tim3, true);
  timerAlarmWrite(timer3, TIM3_Cycle, true);
  //timerAlarmEnable(timer3);
  


}
